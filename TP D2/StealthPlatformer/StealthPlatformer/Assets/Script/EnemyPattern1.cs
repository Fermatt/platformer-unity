﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPattern1 : MonoBehaviour {

	public Transform objetivo;
	public float distancia;
	public float velocidad;
	public float rango;
	float timer;


	private void Update()
	{
		Follow ();
		/*
		if(tipo != 3)
		if (dist < distOffset)
			offset = offset.normalized * dist;
		if (tipo == 0 || tipo == 3)
			transform.position += offset;
		if(tipo == 1)
		if(velocidad > 0 || dist < distancia)
			transform.position += offset;
		if (tipo == 2)
		if (dist < distancia)
			transform.position += offset;
		else if(dist > distancia2)
			transform.position -= offset;
*/
		//--------------- o -----------------//

		//transform.position = Vector3.MoveTowards ();
	}

	void Follow()
	{
		Vector3 diff = objetivo.position - transform.position;
		diff.y = 0;
		Vector3 dir = diff.normalized;
		Vector3 offset = dir * velocidad * Time.deltaTime;
		float dist = diff.magnitude;
		float distOffset = offset.magnitude;
		if (dist < distOffset)
			offset = offset.normalized * dist;
		if (Vector3.Dot (transform.forward, dir) > rango && (objetivo.position - transform.position).magnitude < dir.magnitude*4)
			transform.position += offset;
		else
			Movement ();
	}

	void Movement()
	{
		timer += Time.deltaTime;
		if (timer > 3)
		{
			transform.Rotate (transform.up, 180);
			timer = 0;
		}
		transform.position += transform.forward * velocidad/2 * Time.deltaTime;
	}
}

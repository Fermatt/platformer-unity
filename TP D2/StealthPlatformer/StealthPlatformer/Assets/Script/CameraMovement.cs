﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	[SerializeField] private GameObject player;
	[SerializeField] private int rotateAngle;

	private Vector3 offset;

	void Start () 
	{
		offset = transform.position - player.transform.position;
	}
		
	void LateUpdate () 
	{
		transform.position = player.transform.position + offset;
		float a = Input.GetAxis("HorizontalM");
		if (a < 0 || a > 0)
		{
			transform.RotateAround(player.transform.position, Vector3.up, rotateAngle * a * Time.deltaTime);
			offset = transform.position - player.transform.position;
		}
	}
}
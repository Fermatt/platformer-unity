﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	[SerializeField] private float velocity;
	[SerializeField] private float velocityJump;
	[SerializeField] private Transform c;

	Animator anim;
	Rigidbody rb;
	float velAnim;
	float velI;
	float velK;
	bool isPunching = false;
	bool isCrouching = false;
	float timer;

	// Use this for initialization
	void Awake () {
		anim = GetComponent <Animator> ();
		rb = GetComponent <Rigidbody> ();
	}

	void FixedUpdate()
	{
		Movement ();
		Punch ();
		Animation ();
	}

	void Movement()
	{
		float i = Input.GetAxis("Vertical");
		float k = Input.GetAxis ("Horizontal");
		float vel = (System.Math.Abs (i) + System.Math.Abs (k));
		velAnim = Mathf.Lerp (velAnim, vel, 0.1f);
		//velI = Mathf.Lerp (velI, i, 0.05f);
		//velK = Mathf.Lerp (velK, k, 0.05f);
		transform.LookAt(transform.position + c.right * k + c.forward * i);
		transform.position += transform.forward * vel * Time.deltaTime * velocity;
		if(Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Jump"))
			rb.AddForce(transform.up * velocityJump);
		if (Input.GetKeyDown (KeyCode.LeftControl))
		{
			if (isCrouching)
				isCrouching = false;
			else
				isCrouching = true;
		}
	}

	void Punch()
	{
		if (Input.GetMouseButtonDown (0) && !isPunching)
		{
			isPunching = true;
			timer = 0;
		}
		if (isPunching)
		{
			timer += Time.deltaTime;
			if (timer > 1)
			{
				isPunching = false;
			}
		}

	}

	void Animation()
	{
		anim.SetFloat("Velocity", velAnim);
		anim.SetFloat ("Fall", rb.velocity.y);
		anim.SetBool ("Punch", isPunching);
		anim.SetBool ("Crouch", isCrouching);
	}
}
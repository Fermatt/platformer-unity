﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPattern2 : MonoBehaviour {

	public Transform target;
	public float rango;
	float timer;

	private void Awake()
	{
		//Mathf.Cos (rango);
	}

	private void Update()
	{
		Vector3 dir = Vector3.Normalize (target.position - transform.position);
		if (Vector3.Dot (transform.forward, dir) > rango && (target.position - transform.position).magnitude < dir.magnitude * 4) {
			transform.rotation = Quaternion.LookRotation (dir);
		}
		else
		{
			timer += Time.deltaTime;
			if (timer > 3)
			{
				transform.Rotate (transform.up, 180);
				timer = 0;
			}
		}
	}
}
